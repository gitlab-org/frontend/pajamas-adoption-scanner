import { readFile } from 'node:fs/promises';
import { join } from 'node:path';
import memoize from 'lodash/memoize.js';

// TODO: Why isn't eslint-plugin-import finding this?
// eslint-disable-next-line import/no-unresolved
import { parse } from '@gitlab/codeowners';

const codeowners = await parse(join(import.meta.dirname, 'GITLAB_CODEOWNERS'));

/**
 * Returns the group label given a file path.
 *
 * Uses the `lib/GITLAB_CODEOWNERS` file as the source of truth. If multiple
 * groups match, only the first owner is returned.
 *
 * See https://docs.gitlab.com/ee/user/project/codeowners/ for details on
 * CODEOWNERS syntax.
 *
 * @param {string} path A file path
 * @returns {string} A group label
 */
export const groupForPath = memoize((path) => codeowners.getOwners(path)[0]);

/**
 * Returns the group metrics summary for a given scan file.
 *
 * @param {string} filePath A file path
 * @returns {Promise<Object>} The group metrics summary
 */
export async function groupMetricsForScan(filePath) {
  const scan = JSON.parse(await readFile(filePath, 'utf-8'));

  // NOTE: This is similar to groupRulesTableDataProps in
  // dashboard/lib/rules_table.js. It's different enough that extracting
  // a reusable util probably doesn't make sense.
  const itemsMap = [...scan.results].reduce((acc, finding) => {
    const { group } = finding.extra;
    if (!acc.has(group))
      acc.set(group, {
        name: group,
        notAdopted: 0,
        adopted: 0,
      });

    const item = acc.get(group);

    // Note: Warnings are ignored, since by definition they do not have
    // migration paths, i.e., are not actionable.
    if (finding.extra.severity === 'INFO') {
      item.adopted += 1;
    } else if (finding.extra.severity === 'ERROR') {
      item.notAdopted += 1;
    }

    return acc;
  }, new Map());

  const collator = new Intl.Collator('en');

  return {
    aggregatedAt: new Date().toISOString(),
    minimumFindings: 10,
    bounds: {
      low: 0.8,
      high: 0.95,
    },
    groups: [...itemsMap.values()].sort((a, b) => collator.compare(a.name, b.name)),
  };
}
