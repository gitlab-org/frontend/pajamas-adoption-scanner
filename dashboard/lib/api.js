import { makeUrl } from './url';
import { memoize, memoizeBackedBySessionStorage } from './memoize';

const baseFetch = async (path) => {
  const response = await fetch(path);

  if (!response.ok) {
    const error = new Error(`"${response.url}": ${response.statusText}`);
    error.response = response;
    throw error;
  }

  return response;
};

const fetchJSON = async (path) => {
  const response = await baseFetch(path);

  return response.json();
};

/**
 * Get the data structure describing existing scans.
 */
export const getScansSummary = memoize(() => fetchJSON('scans.json'));

/**
 * Get the data structure describing current rules.
 * @returns {Promise<Map<string, Rule>>} A map of rule ids to rule definitions.
 */
export const getRules = memoize(async () => {
  const rules = await fetchJSON('rules.json');
  return rules.reduce((acc, rule) => {
    acc.set(rule.id, rule);
    return acc;
  }, new Map());
});

/**
 * Decorate scan data with extra fields useful for display logic.
 */
export const enhanceScan = (scan) => {
  scan.results.forEach((finding, i) => {
    finding.id = i;
  });

  return scan;
};

/**
 * Returns the JSON output of a semgrep scan, with some modifications.
 *
 * Modifications:
 * - Each finding is given a unique `id`.
 */
export const getCurrentScan = async () => {
  const { current } = await getScansSummary();

  return enhanceScan(await fetchJSON(current.file));
};

/**
 * Issue search. Use the memoized version instead of this one.
 *
 * Gets all pages for the given query parameters.
 *
 * @param {Object} [params] URL parameters to pass to the endpoint
 * @param {string} [options.projectFullPath] Full path of the project to search in
 * @returns {Promise<Array<Object>>} A promise of an array of issues returned
 */
async function* issueSearch({ projectFullPath = 'gitlab-org/gitlab', ...params } = {}) {
  const projectId = encodeURIComponent(projectFullPath);
  const baseUrl = `https://gitlab.com/api/v4/projects/${projectId}/issues`;
  let page = 1;

  while (page) {
    const pageResponse = await baseFetch(makeUrl(baseUrl, { per_page: 100, ...params, page }));
    const pageOfIssues = await pageResponse.json();

    page = Number(pageResponse.headers.get('X-Next-Page'));

    for (const issue of pageOfIssues) {
      yield {
        url: issue.web_url,
        iid: issue.iid,
        title: issue.title,
        state: issue.state,
        labels: issue.labels,
      };
    }
  }
}

/**
 * Memoized issue search.
 *
 * @param {Object} [params] URL parameters to pass to the endpoint
 * @returns {Promise<Array<Object>>} A promise of an array of issues returned from the API
 */
const memoizedIssueSearch = memoizeBackedBySessionStorage(async (...args) => {
  const issues = [];

  for await (const issue of issueSearch(...args)) {
    issues.push(issue);
  }

  return issues;
});

/**
 * Search for an issue. Uses sessionStorage by default as a cache for results.
 *
 * @param {Object} options
 *     finding.
 * @param {string} options.componentLabel The component label associated with the
 *     finding.
 * @param {string} options.path The path of the finding's file.
 * @returns {{ iid: string, url: string }}
 */
export async function cachedFindIssueForPathAndLabel({ componentLabel, rule, path, noCache }) {
  const searchParams = { labels: componentLabel };
  if (noCache) memoizedIssueSearch.cache.delete(searchParams);

  const issueCandidates = (await memoizedIssueSearch(searchParams))
    .filter((issue) => issue.title.includes(path))
    .sort((a, b) => {
      // Least importantly, issues with pajamas::integrate label first
      const label = 'pajamas::integrate';
      const aHasLabel = a.labels.includes(label);
      const bHasLabel = b.labels.includes(label);

      // prettier-ignore
      return aHasLabel === bHasLabel ?
        0
      : aHasLabel ?
        -1
      : 1;
    })
    .sort((a, b) =>
      // Then open issues first
      // prettier-ignore
      a.state === b.state ?
        0
      : a.state === 'opened' ?
        -1
      : 1,
    )
    .sort((a, b) => {
      // Most importantly, issues with rule in title first
      const aHasRuleInTitle = a.title.includes(rule);
      const bHasRuleInTitle = b.title.includes(rule);

      // prettier-ignore
      return aHasRuleInTitle === bHasRuleInTitle ?
        0
      : aHasRuleInTitle ?
        -1
      : 1;
    });

  const [issue] = issueCandidates;

  return issue ?? null;
}
