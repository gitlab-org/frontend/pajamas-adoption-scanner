module.exports = {
  clearMocks: true,
  restoreMocks: true,
  moduleFileExtensions: ['js', 'json', 'vue'],
  moduleNameMapper: {
    '^~/(.*)$': '<rootDir>/$1',
    '\\.(jpg|jpeg|png|svg|css)$': '<rootDir>/test_support/file_mock.js',
  },
  transform: {
    '^.+\\.js$': 'babel-jest',
    '^.+\\.vue$': '@vue/vue2-jest',
  },
  transformIgnorePatterns: ['node_modules/(?!@gitlab/ui)'],
};
