#!/usr/bin/env python3
import os
from datetime import date, timedelta
from math import floor

today = date.today()
year, week, _ = today.isocalendar()
most_recent_monday = date.fromisocalendar(year, week, 1)

num_weeks = int(os.environ.get("NUM_WEEKS_TO_SCAN", 26))
# Note: parallel nodes are one-indexed, so we subtract one
this_node = int(os.environ.get("CI_NODE_INDEX", 1)) - 1
total_nodes = int(os.environ.get("CI_NODE_TOTAL", 1))

step = floor(num_weeks / total_nodes)
start = this_node * step
stop = min(start + step, num_weeks)

for weeks in range(start, stop):
    historical_date = most_recent_monday - timedelta(weeks=weeks)
    print(historical_date.isoformat())
