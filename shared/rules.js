/**
 * Returns the fully-qualified rule ID's original short id. If the given id is
 * already short, it returns it as-is.
 *
 * For example:
 *
 *     shortId('pas.rules.components.button.okay-button') === 'okay-button'
 *     shortId('bad-button') === 'bad-button'
 *
 * @param {string} ruleId A fully-qualified rule ID.
 * @returns {string} A short id.
 */
export const shortId = (ruleId) => {
  const index = ruleId.lastIndexOf('.');
  return ruleId.slice(index + 1);
};
