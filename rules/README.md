# Component rules

[[_TOC_]]

Have a look at the semgrep documentation on [how to write custom rules](https://semgrep.dev/docs/writing-rules/overview/)

Rule severities have the following meanings:

1. `ERROR`: a rule that detects a pattern that we know is bad, and we know there is
   a better alternative. Such rules must link to an epic, which ideally has in
   its description a migration guide. Complex components, or tricky ones like
   dropdowns, may not have a complete migration guide. These affect the
   adoption percentages of groups.
1. `WARNING`: a rule that detects a pattern that is a code smell, but for which
   we do not have a better alternative yet. These do _not_ affect adoption
   percentages of groups.
1. `INFO`: a rule that detects a pattern indicating adoption of a Pajamas
   component. These are mostly used to track Pajamas adoption over time, and
   affect adoption percentages of groups.

Rules should be written in files specific to their component and source file
type. For instance:

```
rules/components/accordion/
├── accordion-haml-rb.haml
├── accordion-haml-rb.yml
├── accordion-vue-js.vue
└── accordion-vue-js.yml
```

The `accordion-vue-js.yml` file has [exactly one][semgrep upgrade] test file of
the same base name: `accordion-vue-js.vue`. The YAML file contains rules that
use the generic language parser, and target `*.vue` and/or `*.js` files. The
tests must go in the `.vue` file, since that can contain both HTML and
JavaScript.

It is similar for the `accordion-haml-rb.*` files. The YAML file contains rules
that use the generic language parser, and target `*.haml` and/or `*.rb` files.
The tests must go in the `.haml` file, since that can contain both HAML and
Ruby.

If a rule uses a language-specific parser, it must have its own YAML and test
file base name, e.g., `foobar-rb.yml` and `foobar-rb.rb` for rules that are for
Ruby files only.

Paths used in rules (e.g., `exclude`) must be written relative to the GitLab repository root.

## Linting

There are rule linters that run against our rules. Run them locally with:

```
yarn lint:rules
```

This is run in CI in the `node linters` CI job.

## Examples

### ERROR rule

The message should hint towards correct usage and might contain caveats.

```yaml
- id: banner-class-vue
  patterns:
    - pattern-regex: gl-banner
    - pattern-not-regex: </?gl-banner
  message: |
    Detected usage of `gl-banner` class inside JS/Vue file.
    Please use the GlBanner component from `@gitlab/ui`.
  languages:
    - generic
  severity: ERROR
  paths:
    include:
      - '*.js'
      - '*.vue'
    exclude:
      - 'app/assets/javascripts/alert_handler.js'
  metadata:
    needsEpicOrWarning: true
    componentLabel: 'component:banner'
    pajamasCompliant: false
```

### WARNING rule

```yaml
- id: form-input-custom-implementation-haml
  pattern-either:
    - pattern-regex: text_field(?:_tag)?\b
    - pattern-regex: number_field?\b
    - patterns:
        - pattern-inside: '%input ... {...}'
        - pattern-regex: 'type: ["'':]text'
  message: |
    Detected usage of custom input implementation inside HAML file.
  languages:
    - generic
  severity: WARNING
  paths:
    include:
      - '*.haml'
  metadata:
    componentLabel: 'component:form'
    pajamasCompliant: false
```

### INFO rule

```yaml
- id: okay-banner-vue-component
  patterns:
    - pattern-inside: import ... from '@gitlab/ui'
    - pattern: GlBanner
  message: Correct usage of GlBanner gitlab-ui component.
  languages:
    - generic
  severity: INFO
  paths:
    include:
      - '*.js'
      - '*.vue'
  metadata:
    componentLabel: 'component:banner'
    pajamasCompliant: true
```

[semgrep upgrade]: https://gitlab.com/gitlab-org/frontend/pajamas-adoption-scanner/-/merge_requests/316)
